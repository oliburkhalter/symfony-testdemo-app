<?php

namespace App\Service;

class NewsTitleDateSuffixTransformer implements NewsTitleTransformer
{
    public function transformTitle(string $currentTitle): string
    {
        $now = new \DateTimeImmutable('now');
        return "{$currentTitle} - {$now->format('Y-m-d H:i:s')}";
    }
}