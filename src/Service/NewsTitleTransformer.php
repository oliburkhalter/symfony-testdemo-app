<?php

namespace App\Service;

interface NewsTitleTransformer
{
    public function transformTitle(string $currentTitle);
}