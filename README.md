# Symfony Test Demo News App

## Local setup

    # Install dependencies
    composer install

    # Start database with Docker
    docker-compose up -d

    # Install testdata
    bin/console doctrine:migrations:migrate

    # Access Adminer
    http://localhost:7080/
    
    # Start webapp
    symfony server:start
    
    # Access webapp
    http://localhost:8000