<?php

namespace App\Tests\Service;

use App\Service\NewsTitleDateSuffixTransformer;
use PHPUnit\Framework\TestCase;

class NewsTitleTransformerTest extends TestCase
{
    public function testTransformTitle()
    {
        // Given:
        $testee = new NewsTitleDateSuffixTransformer();
        $currentNewsTitle = "Crazy website";

        // When:
        $result = $testee->transformTitle($currentNewsTitle);

        // Then:
        $this->assertStringContainsString('Crazy website - ', $result);
    }
}
